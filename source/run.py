# navigate to the directory containing this file in cmd & run:
# python run.py

from app_name import app
from waitress import serve
from os import environ

if __name__ == '__main__':
    #app.run()
    serve(app, host='0.0.0.0', port=environ.get("PORT", 5000))
