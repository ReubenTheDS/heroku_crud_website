# use official Docker Python image
FROM python:3.7-bullseye

# install virtualenv for use
RUN python -m pip install virtualenv

# create a new user since we should not run as root
RUN useradd -m user1

# switch to that user & home dir
USER user1
WORKDIR /home/user1

# create a virtual env
RUN virtualenv venv1

# activate the venv1
RUN . venv1/bin/activate

# copy the file "requirements.txt" containing dependencies, from the repo to the image
COPY requirements.txt .

# pip install the dependencies
RUN python -m pip install -r requirements.txt  --disable-pip-version-check

# copy the app folder from the repo to the image
COPY ./source  ./source

# expose PORT 5000 since waitress will be serving on that port
EXPOSE 5000/tcp

# run the waitress-flask app
ENTRYPOINT ["python", "source/run.py"]
